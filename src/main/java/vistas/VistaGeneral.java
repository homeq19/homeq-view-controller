package vistas;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;

public class VistaGeneral {

	private JFrame frame;
	private JTable tablaPacientes;
	private DefaultTableModel modelPacientes;
	private String[] nombreColumnas = {"Nombre", "Apellido"};

	
	public VistaGeneral() {
		super();
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(280, 200, 742, 408);
		frame.setTitle("HomeQ");
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 27, 716, 275);
		frame.getContentPane().add(scrollPane);
		
		modelPacientes = new DefaultTableModel(null,nombreColumnas){
			private static final long serialVersionUID = 1L;
			@Override
				public boolean isCellEditable(int row, int column){
				return false;
				}
			};
		tablaPacientes = new JTable(modelPacientes);
		scrollPane.setViewportView(tablaPacientes);	
		
		JButton btnEstado = new JButton("Visualizar estado");
		btnEstado.setBounds(243, 313, 140, 37);
		frame.getContentPane().add(btnEstado);
		
	}
	
	public void mostrarVentana() {
		frame.setVisible(true);
	}

	public DefaultTableModel getModelPacientes() {
		return modelPacientes;
	}
}
