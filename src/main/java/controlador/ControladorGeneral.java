package controlador;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import dto.Paciente;
import dto.Sitio;
import json.ArchivosJSON;
import observer.Confinamiento;
import vistas.VistaGeneral;

public class ControladorGeneral {
	
	private VistaGeneral vistaGeneral;
	private Confinamiento confinamiento;
	private ArrayList<Paciente> pacientes;
	
	public ControladorGeneral(VistaGeneral vista) {
		this.vistaGeneral = vista;
		this.pacientes = ArchivosJSON.leerJSONPacientes();
		this.confinamiento = new Confinamiento(pacientes);
	}

	
	public void inicializar() {
		this.vistaGeneral.mostrarVentana();
		llenarTabla();
		
	}
	

	public void llenarTabla()  {		
		this.vistaGeneral.getModelPacientes().setRowCount(0);
		for (int i = 0; i< pacientes.size(); i++) {
			Object[] fila = { 
						pacientes.get(i).getNombre(), 
						pacientes.get(i).getApellido()
			};
		
	
			this.vistaGeneral.getModelPacientes().addRow(fila);
		}
		}
//		
	}


