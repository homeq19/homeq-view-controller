package main;

import controlador.ControladorGeneral;
import vistas.VistaGeneral;

public class Main {
	
	public static void main(String[] args)  {
		VistaGeneral vista = new VistaGeneral();
		ControladorGeneral controlador  = new ControladorGeneral(vista);
		controlador.inicializar();
	}

}
